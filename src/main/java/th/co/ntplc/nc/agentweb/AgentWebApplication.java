package th.co.ntplc.nc.agentweb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@Configuration
@ComponentScan(basePackages = "th.co.ntplc.nc.*") //Scan All Class Is A Component (Scan Package And Sub-package)
@EnableJpaRepositories("th.co.ntplc.nc.repository") //Opening JPARepository and Config All JPA can create instance Auto
@EntityScan("th.co.ntplc.nc.entity") //Scan All Entity in Package Entity
public class AgentWebApplication{
	
	

	public static void main(String[] args) {
		SpringApplication.run(AgentWebApplication.class, args);
		
	}

	

	
	


}
