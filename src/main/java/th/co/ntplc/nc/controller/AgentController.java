package th.co.ntplc.nc.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import th.co.ntplc.nc.entity.AgentEntity;
import th.co.ntplc.nc.repository.AgentRepository;

@Controller
public class AgentController {
	
	@Autowired
	private AgentRepository repository;
	
	@GetMapping("/")
	public String homePage(Model model) {
		model.addAttribute("agentlist",repository.findAll());
		return "home";
	}
	
	@GetMapping("/saveAgentPage")
	public String saveAgentPage(Model model) {
		AgentEntity agentEntity=new AgentEntity();
		model.addAttribute("agent",agentEntity);
	 return "add_agent";	
	}
	
	@PostMapping("/saveAgent")
	public String saveAgent(@ModelAttribute("agent") AgentEntity agent) {
		repository.save(agent);
		return "redirect:/";
	}
	
	@GetMapping("/updateAgentPage/{agentId}")
	public String showUpdateAgentPage(@PathVariable("agentId") int id, Model model) {
		Optional<AgentEntity> temp = repository.findById(id); //Optional = wrapper object
		AgentEntity agentEntity = temp.get();
		model.addAttribute("agent", agentEntity);
		return "update_Agent";
	}
	
	@GetMapping("/deleteAgent/{agentId}")
	public String deleteAgent(@PathVariable("agentId") int id) {
		repository.deleteById(id);
		return "redirect:/";
	}
	
	
}
