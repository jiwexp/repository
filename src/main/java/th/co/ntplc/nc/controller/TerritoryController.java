//package th.co.ntplc.nc.controller;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Controller;
//import org.springframework.ui.Model;
//import org.springframework.web.bind.annotation.GetMapping;
//
//import th.co.ntplc.nc.entity.TerritoryEntity;
//
//@Controller
//public class TerritoryController {
//
//	@Autowired
//	private TerritoryEntity repo;
//	
//	@GetMapping("/")
//	public String homePage(Model model) {
//		model.addAttribute("territorylist", repo.getTerritoryName());
//		return "home";
//	}
//}
