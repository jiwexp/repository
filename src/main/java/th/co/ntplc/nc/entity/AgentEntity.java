package th.co.ntplc.nc.entity;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

@Entity
@Table(name = "agents")
public class AgentEntity {

	
	@Id
	@GeneratedValue(strategy = GenerationType.TABLE)
	private int agentId;
	private String agentName;
	private String agentLastName;

	
	
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.DETACH)
	@JoinColumn(name="territoryId",referencedColumnName = "territoryId")
	private TerritoryEntity territoryEntity;

	public AgentEntity() {
		super();
		// TODO Auto-generated constructor stub
	}

	public AgentEntity(String agentName, String agentLastName, int territoryId, TerritoryEntity territoryEntity) {
		super();
		this.agentName = agentName;
		this.agentLastName = agentLastName;
		this.territoryEntity = territoryEntity;
	}

	public int getAgentId() {
		return agentId;
	}

	public void setAgentId(int agentId) {
		this.agentId = agentId;
	}

	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public String getAgentLastName() {
		return agentLastName;
	}

	public void setAgentLastName(String agentLastName) {
		this.agentLastName = agentLastName;
	}

	public TerritoryEntity getTerritoryEntity() {
		return territoryEntity;
	}

	public void setTerritoryEntity(TerritoryEntity territoryEntity) {
		this.territoryEntity = territoryEntity;
	}

	@Override
	public String toString() {
		return "AgentEntity [agentId=" + agentId + ", agentName=" + agentName + ", agentLastName=" + agentLastName
				+ ", territoryEntity=" + territoryEntity + "]";
	}

	
	
	
	
	
	
}
