package th.co.ntplc.nc.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import th.co.ntplc.nc.entity.AgentEntity;

@Repository
public interface AgentRepository extends JpaRepository<AgentEntity, Integer> {
	

}