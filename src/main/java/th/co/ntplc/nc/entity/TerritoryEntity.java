package th.co.ntplc.nc.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name = "territorys")
public class TerritoryEntity {
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.TABLE)
	private int territoryId;
	private String territoryName;
	
	public TerritoryEntity() {
		super();
		// TODO Auto-generated constructor stub
	}
	public TerritoryEntity(String territoryName) {
		super();
		this.territoryName = territoryName;
	}
	public int getTerritoryId() {
		return territoryId;
	}
	public void setTerritoryId(int territoryId) {
		this.territoryId = territoryId;
	}
	public String getTerritoryName() {
		return territoryName;
	}
	public void setTerritoryName(String territoryName) {
		this.territoryName = territoryName;
	}
	@Override
	public String toString() {
		return "TerritoryEntity [territoryId=" + territoryId + ", territoryName=" + territoryName + "]";
	}
	
}
